from django import template
from django.template.defaultfilters import stringfilter
from django.urls import reverse
from django.utils.html import format_html

from locdata.models import Locale

register = template.Library()

@register.filter(is_safe=True)
@stringfilter
def link_locale(value):
    try:
        loc = Locale.objects.get(code=value)
        url = reverse('locale', args=[value])
        return format_html('<a href="{}">{}</a>', url, value)
    except Locale.DoesNotExist:
        return value

@register.filter
def as_list(value):
    return [value]

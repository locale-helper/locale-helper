import os
import sys
from xml.etree.ElementTree import ElementTree

from django.core.management.base import BaseCommand

from locdata.models import Category, Locale, Value

class Command(BaseCommand):
    def handle(self, *args, **options):
        if len(args) < 1:
            print "This command take an xml file as parameter"
            sys.exit(1)
        file_path = args[0]
        if not os.path.isfile(file_path):
            print "Unable to find file '%s'" % file_path
            sys.exit(1)
        loc_code = file_path.split('/')[-1].split('.', 1)[0]
        try:
            loc = Locale.objects.get(code=loc_code)
        except Locale.DoesNotExist:
            print "Unable to find an existing locale with '%s' as code" % loc_code
            sys.exit(1)
        # FIXME: origin
        origin = 'afrigen'

        tree = ElementTree()
        tree.parse(file_path)
        root = tree.getroot()
        
        months_abbr={}; months_wide={}; days_abbr={}; days_wide={}; am_pm={}
        def traverse(parent_path, elem):
            for child in elem.getchildren():
                path = "%s/%s" % (parent_path, child.tag)
                if child.attrib:
                    key = child.keys()[0]
                    path += "[@%s='%s']" % (key, child.attrib[key])
                if not child.getchildren() and child.text and child.text.strip() != "":
                    # Text content, try to import
                    value = child.text.strip()
                    if path.startswith("/ldml/dates/calendars/calendar[@type='gregorian']/months/monthContext[@type='format']/monthWidth[@type='abbreviated']/month"):
                        months_abbr[child.attrib['type']] = value
                    elif path.startswith("/ldml/dates/calendars/calendar[@type='gregorian']/months/monthContext[@type='format']/monthWidth[@type='wide']/month"):
                        months_wide[child.attrib['type']] = value
                    elif path.startswith("/ldml/dates/calendars/calendar[@type='gregorian']/days/dayContext[@type='format']/dayWidth[@type='abbreviated']/day"):
                        days_abbr[child.attrib['type']] = value
                    elif path.startswith("/ldml/dates/calendars/calendar[@type='gregorian']/days/dayContext[@type='format']/dayWidth[@type='wide']/day"):
                        days_wide[child.attrib['type']] = value
                    elif path == "/ldml/dates/calendars/calendar[@type='gregorian']/am":
                        am_pm['am'] = value
                    elif path == "/ldml/dates/calendars/calendar[@type='gregorian']/pm":
                        am_pm['pm'] = value
                    else:
                        try:
                            cat = Category.objects.get(cldr_path=path)
                        except Category.DoesNotExist:
                            print "Unable to import '%s' (value '%s')" % (path, value)
                            continue
                        newval = Value.objects.create(locale=loc, category=cat, value=value, origin=origin)
                    
                traverse(path, child)
        traverse("/%s" % root.tag, root)
        if months_abbr:
            cat = Category.objects.get(posix_name='abmon')
            value = u"\n".join([months_abbr[str(i)] for i in range(1, 13)])
            newval = Value.objects.create(locale=loc, category=cat, value=value, origin=origin)
        if months_wide:
            cat = Category.objects.get(posix_name='mon')
            value = u"\n".join([months_wide[str(i)] for i in range(1, 13)])
            newval = Value.objects.create(locale=loc, category=cat, value=value, origin=origin)
        if days_abbr:
            cat = Category.objects.get(posix_name='abday')
            value = u"\n".join([days_abbr[str(i)] for i in ['sun', 'mon', 'tue', 'wed', 'thu', 'fri', 'sat']])
            newval = Value.objects.create(locale=loc, category=cat, value=value, origin=origin)
        if days_wide:
            cat = Category.objects.get(posix_name='day')
            value = u"\n".join([days_wide[str(i)] for i in ['sun', 'mon', 'tue', 'wed', 'thu', 'fri', 'sat']])
            newval = Value.objects.create(locale=loc, category=cat, value=value, origin=origin)
        if am_pm:
            cat = Category.objects.get(posix_name='am_pm')
            newval = Value.objects.create(locale=loc, category=cat, value="%s\n%s" % (am_pm['am'], am_pm['pm']), origin=origin)

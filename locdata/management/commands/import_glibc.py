import codecs
import os
import re
from urllib import request

from django.core.management.base import BaseCommand, CommandError

from locdata.models import ESCAPE_CHAR, Locale, Language, Country, Category, Value


class Command(BaseCommand):
    """ Examples:
        Import locale from local system:
        $ ./manage.py import_glibc --origin='eglibc' /usr/share/i18n/locales/fr_FR [...]
        Import all locales from current glibc repository (HEAD tag)
        ./manage.py import_glibc --origin='glibc-HEAD'
        Import specific locales from current glibc repository (HEAD tag)
        ./manage.py import_glibc --origin='glibc-HEAD' fr_FR es_ES [...]
    """

    def add_arguments(self, parser):
        parser.add_argument('locales', nargs='*')
        parser.add_argument('--origin', dest='origin', help="indicate origin of data")
        parser.add_argument('--start-at', dest='start', help="indicate at which locale to start importing")

    def handle(self, *args, **options):
        self.origin = options.get('origin')
        self.charset = 'utf-8'
        if not self.origin:
            raise CommandError("You must specify data origin with --origin option")
        if self.origin == "glibc-HEAD":
            if options['locales']:
                locales = options['locales']
            else:
                # Read SUPPORTED, extract locale names
                sup_file = read_glibc_file('localedata/SUPPORTED', 'HEAD')
                locales = read_supported(sup_file, self.stderr)
            # Loop over locales
            started = not bool(options.get('start'))
            for loc in locales:
                if loc == 'C':
                    continue
                if not started:
                    if loc == options.get('start'):
                        started = True
                    else:
                        continue
                loc_file = read_glibc_file('localedata/locales/%s' % loc, 'HEAD')
                self.import_file(loc_file, loc)
        else:
            # Take arguments as file names to import
            if not options['locales']:
                raise CommandError("This command takes at least one file path as parameter")

            for file_path in options['locales']:
                if not os.path.isfile(file_path):
                    self.stdout.write("Unable to find file '%s', ignoring" % file_path)
                    continue
                loc_code = file_path.split('/')[-1]
                with open(file_path, 'rb') as fh:
                    self.import_file(fh, loc_code)

    def import_file(self, loc_file, loc_code):
        if loc_code.startswith("iw_"):
            self.stdout.write("iw language code is deprecated, not importing %s locale" % loc_code)
            return
        self.stdout.write("Importing locale %s...\n" % loc_code)
        country_code = ""
        script = ""
        if '_' in loc_code:
            lang_code, country_code = loc_code.split('_')
        else:
            lang_code = loc_code
        if '@' in country_code:
            country_code, script = country_code.split('@')

        defaults = {'language': Language.get_by_code(lang_code) if lang_code != 'i18n' else None}
        if country_code:
            defaults['country'] = Country.objects.get(code=country_code)
        self.loc, created = Locale.objects.get_or_create(
            code=loc_code, script=script,
            defaults=defaults
        )
        self.existing_values_pks = list(Value.objects.filter(locale=self.loc, origin=self.origin
            ).values_list('id', flat=True))

        in_section = None
        in_value = False
        section_names = [cat.posix_name for cat in Category.objects.filter(parent__isnull=True) if cat.posix_name]
        verbatim_sections = ['LC_CTYPE', 'LC_COLLATE']
        comment, value = "", ""
        for i, line in enumerate(loc_file):
            try:
                line = line.decode('utf-8').strip()
            except UnicodeDecodeError:
                self.stdout.write("Warning: non UTF-8 char found in %s, skipped..." % loc_code)
                continue
            if line.startswith('%') or line == '':
                if "Charset:" in line:
                    try:
                        charset = re.match(r'^% Charset:\s*(.*)$', line).groups()[0]
                        codecs.lookup(charset)
                        self.charset = charset
                    except (AttributeError, LookupError):
                        self.stdout.write("Unable to extract valid Charset from line '%s'" % line)
                continue
            # Begin of section
            if not in_section and line in section_names:
                in_section = line
                continue
            # End of section
            if in_section and line[4:] == in_section:
                # Save verbatim section value
                if in_section in verbatim_sections and value:
                    cat = Category.objects.get(posix_name=in_section)
                    if not isinstance(value, str):
                        value = str(value, self.charset)
                    self.update_value(cat, value, comment, 'value')
                    comment, value = "", ""
                in_section = None
                continue
            if not in_section:
                continue
            if in_section and in_section in verbatim_sections:
                if value:
                    value += "\n"
                value += line
                continue
            if in_value:
                value += line
            else:
                try:
                    key, value = line.split(None, 1)
                except ValueError:
                    self.stderr.write("Error on line %d (%s)" % (i, line))
                    continue
            in_value = value.endswith("/")
            if in_value:
                value = value[:-1].strip()  # remove ending slash
                value, _ = split_comment(value)  # Discard comment inside value
                continue

            # Clean value content
            value, comment = split_comment(value)
            if value.startswith('"') and value.endswith('"'):
                value = value[1:-1]
            value = re.sub(r'%c(.)' % ESCAPE_CHAR, r'\1', value)
            if '"' in value:
                value = re.sub(r'"\s*;\s*"', "\n", value)
            if "<U" in value:
                value = re.sub(r'<U([\dA-Fa-f]{4})>', uni_from_hex, value)
            if not isinstance(value, str):
                value = str(value, self.charset)
            if not isinstance(comment, str):
                comment = str(comment, self.charset)
            # Exceptions
            if key == 'category':
                value = ""
                continue
            if key == 'copy':
                cat = Category.objects.get(posix_name=in_section)
                self.update_value(cat, value, comment, 'copy_from')
                comment, value = "", ""
                continue
            # Insert value in DB
            try:
                cat = Category.objects.get(posix_name=key)
            except Category.DoesNotExist:
                self.stderr.write("Unable to import '%s' (value '%s')" % (key, value))
                continue
            self.update_value(cat, value, comment, 'value')
            comment, value = "", ""

        # Remove existing values no longer in file
        if self.existing_values_pks:
            qs = Value.objects.filter(pk__in=self.existing_values_pks).select_related('category')
            self.stdout.write("Removing unfound following values: %s\n" % (
                ', '.join([val.category.name for val in qs]),))
            qs.delete()

    def update_value(self, cat, value, comment, value_field):
        new_val, created = Value.objects.get_or_create(
            locale=self.loc, category=cat, origin=self.origin,
            defaults = {value_field: value, 'comments': comment}
        )
        if not created:
            if new_val.id not in self.existing_values_pks:
                self.stdout.write("Warning: probable duplicate value '%s'" % new_val)
                return
            self.existing_values_pks.remove(new_val.id)
            if (getattr(new_val, value_field) != value or new_val.comments != comment):
                new_val.value = value
                new_val.comments = comment
                new_val.save()
        # If a 'local' value exist and is identical to this one, remove the local value
        if self.origin != 'local':
            try:
                loc_val = Value.objects.get(locale=self.loc, category=cat, origin='local')
                if new_val.value == loc_val.value:
                    loc_val.delete()
            except Value.DoesNotExist:
                pass


def uni_from_hex(match):
    return chr(int(match.group(1), 16))

def split_comment(value):
    # Extract comment from a value and return a tuple (value, comment)
    if not '%' in value:
        return value, ''
    val = ''; comment = ''
    in_comment = in_string = False
    for c in value:
        if c == '"':
            in_string = not in_string
        if c == '%' and not in_string:
            in_comment = True
        if in_comment:
            comment += c
        else:
            val += c
    return val.strip(), comment.strip()

def read_glibc_file(path, tag):
    url = "http://sourceware.org/git/?p=glibc.git;a=blob_plain;f=%s;hb=%s" % (path, tag)
    req = request.Request(url)
    handle = request.urlopen(req)
    return handle

def read_supported(sup_file, stderr):
    locales = []
    for line in sup_file:
        line = line.decode('utf-8')
        if line.startswith('#'):
            continue
        if line.startswith('SUPPORTED-LOCALES'):
            continue
        if line.endswith('\\\n'):
            line = line[:-2].strip()
        if line in ("", "\n"):
            continue
        try:
            locale = re.match(r'([^\.]*)\.?.*/.*', line).groups()[0]
        except AttributeError:
            stderr.write("Unable to extract locale from line '%s'" % line)
            continue
        if not locale in locales:
            locales.append(locale)
    return locales
